package com.company;


import java.util.Random;

public class Generator {


    final String UPPER_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    final String LOWER_LETTERS = UPPER_LETTERS.toLowerCase();
    final String DIGITS = "123456789";

    private Random random = new Random();
    private String[] randomTypes;
    private String[] types = {"String", "Date", "Integer", "Float"};


    public String stringGenerator(int wordLength){

        String alphaSet = UPPER_LETTERS + LOWER_LETTERS;
        char[] charSeq; // из которой будут выбираться сисмволы
        char[] genCharSeq; // в которую будут генерироваться
        charSeq = alphaSet.toCharArray();

        genCharSeq = new char[wordLength];
        for (int i = 0; i < wordLength; i++) {// генерируем символы в зависимости от указанной длины слова
            genCharSeq[i] = charSeq[random.nextInt(alphaSet.length())]; // генерируется очередное случайное число из диапазона нашего набора, теперь оно - индекс по которому из набора символов берется символ
        }
        return new String(genCharSeq);
    }


    public String dateGenerator(int dateLength) {
        char[] digitSeq = DIGITS.toCharArray();
        char[] digitDayMonthSeq = ("0" + DIGITS.substring(0,3)).toCharArray(); // возьмем первые 3 символа из массива чисел-символов для первого числа в дне и месяце

        char[] genCharSeq = new char[dateLength];


        // генерируем день
        genCharSeq[0] = digitDayMonthSeq[random.nextInt(digitDayMonthSeq.length)]; // нет смысла делать цикл на 2 итерации, потому что каждая итерация все-равно обрабатывается по-своему и нужны будут доп условия в цикле
        if(genCharSeq[0] == '3') {
            genCharSeq[1] = digitDayMonthSeq[random.nextInt(2)]; // если это 30е числа, то тут второй символ- '0' либо '1'
        } else{
            genCharSeq[1] = digitSeq[random.nextInt(digitSeq.length)];
        }

        // генерируем месяц
        genCharSeq[2] = '.'; genCharSeq[5] = '.';
        genCharSeq[3] = digitDayMonthSeq[random.nextInt(2)]; // первый символ месяца либо '0' либо '1'
        if(genCharSeq[3] == '1'){
            genCharSeq[4] = digitDayMonthSeq[random.nextInt(3)]; // если месяц начинается с 1, то второй символ либо '0' либо '1' либо '2'
        } else{
            genCharSeq[4] = digitSeq[random.nextInt(digitSeq.length)];
        }

        //генерируем год
        for (int i = 6; i < 10; i++){
            genCharSeq[i] = digitSeq[random.nextInt(DIGITS.length())]; // здесь можно разные цифры
        }

        return new String(genCharSeq);
    }

    public int integerGenerator(int integerLength){
        char[] digitSeq = DIGITS.toCharArray();
        char[] genCharSeq;

        genCharSeq = new char[integerLength];
        for (int i = 0; i < integerLength; i++){
            genCharSeq[i] = digitSeq[random.nextInt(DIGITS.length())];
        }

        return Integer.parseInt(new String(genCharSeq));
    }

    public String floatGenerator(int floatLength){
        char[] digitSeq = DIGITS.toCharArray();
        char[] genCharSeq;

        genCharSeq = new char[floatLength];
        int commaPlace = 1+ random.nextInt(floatLength -2); // запятая разместиться под таким индексом
        for (int i = 0; i < floatLength; i++){
            if(i == commaPlace){
                genCharSeq[i] = ',';
                continue;
            }
            genCharSeq[i] = digitSeq[random.nextInt(DIGITS.length())];
        }

        return new String(genCharSeq);
    }
}
