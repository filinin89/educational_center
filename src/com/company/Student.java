package com.company;

import java.util.ArrayList;


public class Student {


    private String name;
    private String curriculumType;
    private String startDate; // у каждого студента свой старт
    //private int endDate;
    private float possibleScore;
    int currentCourse=0; // счетчик курсов студента
    int currentCourseDuration; // текущий курс студента
    int currentCurriculum;
    int сurriculumDuration;
    int daysLeft;
    private ArrayList<Course> curriculum;
    private ArrayList<Integer> marks = new ArrayList<>();
    int k = 0; // счетчик оценок
    int numberOfMarks;
    int sum = 0; // текущая сумма баллов
    int wholeDuration;
    private float averageScore;
    private Generator gen = new Generator();
    boolean finished;


    public int getWholeDuration() {
        return wholeDuration;
    }

    public void setPossibleScore(float possibleScore) {
        this.possibleScore = possibleScore;
    }

    public float getPossibleScore() {
        return possibleScore;
    }

    public void setCurriculum(ArrayList curriculum) {
        this.curriculum = curriculum;
    }

    public void setAverageScore(float averageScore) {
        this.averageScore = averageScore;
    }


    public ArrayList<Integer> getMarks() {
        return marks;
    }


    public String getName() {
        return name;
    }

    public ArrayList<Course> getCurriculum() {
        return curriculum;
    }

    public String getCurriculumType() {
        return curriculumType;
    }

    public float getAverageScore() {
        return averageScore;
    }


    public Student(int i){

        this.name = MiniDB.studentNames[i]; // пока просто берем очередное имя из введенного списка студентов
        // можно зарандомить, даже подбирать различные комбинации имен и фамилий как в phrase generator
        this.curriculumType = MiniDB.curriculumType[i];
        this.startDate = gen.dateGenerator(10);

    }






}
