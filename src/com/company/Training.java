package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class Training {
    private static Random rand = new Random();
    private ArrayList<Student> students = Main.getStudents();
    private ArrayList<Student> studentsF = new ArrayList<>();
    private ArrayList<Student> studentsS = new ArrayList<>();
    //private ArrayList<Float> averageScoreList = new ArrayList<>();
    private int finished = 0;



    public void training(){

        //ArrayList<Course> curriculum = student.getCurriculum();

        // подготовим необходимые данные для подсчета показателей студента в учебе
        for (Student student : students) {

            for (int i = 0; i < student.getCurriculum().size(); i++) {
                student.wholeDuration += student.getCurriculum().get(i).duration; // для каждого студента посчитаем продолжительность обучения
            }

            student.numberOfMarks = student.wholeDuration / 8; // кол-во оценок,которое можно получить за все обучение
        }


        int initialNumbOfStudents = students.size();

        int i = 0; int days=0;
        finish: while(initialNumbOfStudents > finished) { // 1 день обучения по курсу // идем пока все не закончат
            days++;
            // кол-во итераций следующего цикла
            for (Student student : students) { // каждый студент шагает по одному дню

                if(i < students.size()){ // проинициализируем продолжительность первого курса у каждого студента
                    student.currentCourseDuration = student.getCurriculum().get(student.currentCourse).duration; // длительность текущего курса
                }
                i++;
                student.сurriculumDuration = student.getCurriculum().size(); // длительность программы обучения

                student.getMarks().add(1 + rand.nextInt(5)/*5*/); // в конце дня поставим случайную оценку
                student.sum += student.getMarks().get(student.k); // прибавляем ее к сумме
                System.out.println("• Студент закончил день с оценкой " + student.getMarks().get(student.k));
                student.wholeDuration -= 8; // кол-во оставшихся дней до конца учебной программы
                student.currentCourseDuration -=8; // кол-во дней до конца курса
                System.out.print(student.getName() + " - До окончания обучения по программе " + student.getCurriculumType() + " осталось " + student.wholeDuration + "ч. ");

                student.setAverageScore((float) student.sum / student.getMarks().size()); // посчитаем средний балл
                System.out.println("Текущий средний балл " + String.format("%.1f", student.getAverageScore())); // всю сумму оценок делим на кол-во

                // расчитываем возможность отчисления
                int sum1 = 0; //объявим максимально вероятную сумму
                sum1 += student.sum; // прибавим текущую сумму оценок
                sum1 += 5 * (student.numberOfMarks - student.getMarks().size()); // если студент во все оставшиеся дни получит 5
                float possibleScore = (float) sum1 / student.numberOfMarks;
                student.setPossibleScore(possibleScore);
                if (possibleScore < 4.5) {
                    System.out.println("Студент должен быть отчислен " + String.format("%.2f", possibleScore) + "\n"); //students.remove(student); // обычный remove работать не будет - ConcurrentModificationException, тут надо другое
                } else {
                    System.out.println("Студент может продолжать обучение " + String.format("%.2f", possibleScore) + "\n");
                }
                student.k++; // в другой день другая оценка, у каждого студента свой счетчик оценок
                if(student.currentCourseDuration  <= 0) {
                    student.currentCourse++; // если студент закончил курс, то переключаемся на другой
                    if(student.сurriculumDuration <= student.currentCourse) {// если номер курса становится больше или равен длительности учебной программы, то один студент заканчивает
                        this.finished++;

                        if (possibleScore >= 4.5) System.out.println("Студент успешно закончил обучение!\n ");
                    }else {
                        student.currentCourseDuration = student.getCurriculum().get(student.currentCourse).duration; // получаем нового курса
                    }

                }

                if(initialNumbOfStudents == finished) System.out.println("Студенты успешно окончили обучение");

                // -- Срез --
                // Есть тут кого отчислить?
                if(i%9 == 0){ // срез надо делать не ране чем через кол-во студентов, а то будет путанница в выводе

                    for(Student student1 : students){
                        if(student1.getPossibleScore() >= 4.5){ // добавим всех у кого есть вероятность не отчисления
                            studentsF.add(student1);
                        }

                    }

                    System.out.println("\n\nОтфильтрованный список студентов по вероятности не отчисления(кто остался) - без сортировки по убыванию: ");
                    for(Student student1 : studentsF){
                        System.out.println(student1.getName());
                    }
                    System.out.println();

                    if(studentsF.isEmpty()) {
                        System.out.println("Все студенты отчилены(\nУчеба продлилась " + days + " дней"); // как только все отчислятся обучение закончится
                        break finish;
                    }
                    studentsF.clear();



                    studentsS.addAll(0, students);// добавим все элементы в новый список для сортировки
                    Collections.sort(studentsS, new Comparator<Student>() { // отсортируем по AverageScore
                        public int compare(Student s1, Student s2) {
                            return Float.compare(s1.getAverageScore(), s2.getAverageScore());
                        }
                    });
                    Collections.reverse(studentsS);

                    System.out.println("\nОтсортированный список студентов по убыванию среднего балла ");
                    for(Student student1 : studentsS){
                        System.out.println(student1.getName() + " " + String.format("%.1f", student1.getAverageScore()));
                    }
                    studentsS.clear();



                    studentsS.addAll(0, students);// добавим все элементы в новый список для сортировки
                    Collections.sort(studentsS, new Comparator<Student>() {
                        public int compare(Student s1, Student s2) { 
                            return Float.compare(s1.getWholeDuration(), s2.getWholeDuration());
                        }
                    });

                    System.out.println("\n\nОтсортированный список студентов по времени до окончания обучения ");
                    for(Student student1 : studentsS){
                        System.out.println(student1.getName() + " " + (int)Math.ceil(student1.getWholeDuration()/8) + " дня");
                    }
                    studentsS.clear();
                    System.out.println("\n");





                }







            }
        }

    }
}
