package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class TrainingProgram {

    Random random = new Random();
    Generator gen = new Generator();
    Map curiculums = new HashMap();
    static int curriculumsCount=0;


    public ArrayList<Course> composeCurriculum(String curriculumType){

        ArrayList<Course> curriculum = new ArrayList<>(); // курс и его продолжительность
        int numberCourses;
        if(curriculumsCount >= MiniDB.hardcodeCourses.length) { // у заданных в коде курсов своя длина, им ее случайную не нужно
            numberCourses = 5 + random.nextInt(5); // случайное кол-во курсов в указанном диапазоне
        }else{
            numberCourses = MiniDB.hardcodeCourses[curriculumsCount].length;
        }
        
        for(int i=0; i< numberCourses; i++){
            int courseDuration = 8 + random.nextInt(40);

            // первые 2 курса заполнятся осознанными значениями
            switch(curriculumType)
            {
                case "J2EE Developer" :{
                    curriculum.add(new Course(MiniDB.hardcodeCourses[curriculumsCount][i], courseDuration)); // добавляем курсы в список курсов - учебный план
                    break;
                }
                case "JavaDeveloper" :{
                    curriculum.add(new Course(MiniDB.hardcodeCourses[curriculumsCount][i], courseDuration));
                    break;
                }
                // остальные составим рандомно
                default: curriculum.add(new Course(gen.stringGenerator(10), courseDuration));
            }


        } // по выходе из этого цикла один список курсов, т.е. одна учебная программа готова

        curriculumsCount++; // этот счетчик только для заданных в коде учебных программ
        curiculums.put(curriculumType, curriculum);  // добавим список курсов по ключу - учебная программа в мап

        return curriculum;
    }




}
